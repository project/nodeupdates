Node Updates
Written by Arche Twist, http://archetwist.com

Provides a way to keep anonymous visitors updated about changes to a project
or page they are interested in. This is done by publishing descriptions
of the updates, "changelogs" (or any other messages) through special
per-node RSS feeds created by the module.

It is an ideal solution for frequently updated, popular webpages
with a large number of anonymous visitors.

By providing an update message at the node edit form administrators can
notify node feed subscribers of the updated content. The feed is created
automatically after the first update message has been submitted.

Installation and usage
======================

1) Upload and enable the module.
2) Create a new node or edit existing one. At the submit/edit form you will
   find a new group of options, >>Update message<<.
3) Enter a new message (title is not mandatory) and submit the form.
4) The RSS feed will be created automatically. The path to the feed is the
   node's path preceeded by updates/, e.g. http://example.com/updates/node/1.
5) You can later add new messages to the feed or delete existing ones.
   All is done using the node edit form.